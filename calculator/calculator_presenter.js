import {CalculatorService} from "./calculator_service.js";
import {CalculatorView} from "./calculator_view.js";

export class CalculatorPresenter {
    #calculatorService = new CalculatorService();
    #calculatorView = new CalculatorView();

    constructor() {
        this.#initListeners();
    }

    #initListeners() {
        this.#addClickEventListeners(this.#calculatorView.operatorGenerator, this.#handleOperator.bind(this));
        this.#addClickEventListeners(this.#calculatorView.digitGenerator, this.#handleDigit.bind(this));
    }

    #addClickEventListeners(generator, handlerFn) {
        for (let htmlElement of generator) {
            (htmlElement || new HTMLElement()).addEventListener('click', ({target}) => {
                const key = (target || new HTMLElement()).innerText
                handlerFn(key);
            });
        }
    }

    #handleOperator(operatorKey) {
        this.#calculatorService.handleOperator(operatorKey);
        this.#updateView();
    }

    #handleDigit(digitKey) {
        this.#calculatorService.handleDigit(digitKey);
        this.#updateView();
    }

    #updateView() {
        this.#calculatorView.updateValueContent(this.#calculatorService.activeValue);
    }
}
