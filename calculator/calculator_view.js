function getElementByClassName(className) {
    return (document.getElementsByClassName(className) || document.createDocumentFragment().children).item(0);
}

function* elementGenerator(className) {
    const buttons = document.getElementsByClassName(className);
    for (let i = 0; i < buttons.length; i++) {
        yield buttons.item(i);
    }
}

const OPERATOR_CLASS = 'operator';
const DIGIT_CLASS = 'digit';
const VALUE_CLASS = 'value-wrapper';

export class CalculatorView {
    #valueElement = Object.create(HTMLElement.prototype, {});

    get operatorGenerator() {
        return elementGenerator(OPERATOR_CLASS);
    }

    get digitGenerator() {
        return elementGenerator(DIGIT_CLASS);
    }

    constructor() {
        this.#init();
    }

    #init() {
        this.#valueElement = getElementByClassName(VALUE_CLASS);
    }

    updateValueContent(content) {
        this.#valueElement.innerText = content;
    }
}
