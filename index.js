import {CalculatorPresenter} from "./calculator/calculator_presenter.js";

class Starter {
    static #presenter;

    static start() {
        if (!this.#presenter) {
            this.#presenter = new CalculatorPresenter();
        }
    }
}

Starter.start();
