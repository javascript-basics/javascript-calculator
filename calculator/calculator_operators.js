export class Operator {
    #value;
    calculateWithEveryOperator = false;
    calculateCurrentOperators = false;

    updateCurrentOperator = false;
    onlyUpdateValue = false;

    needDigit = false;
    needToReset = false;
    needToResetAfterDigit = false;
    changeOperatorOrder = false;

    get value() {
        return this.#value;
    }

    set value(value) {
        this.#value = value;
    }

    constructor(value) {
        this.#value = value;
    }

    calculate(operator) {
        return this.#value;
    }
}

export class SumOperator extends Operator {
    constructor(value) {
        super(value);
        this.needDigit = true;
        this.calculateWithEveryOperator = true;
    }

    calculate(operator) {
        return this.value + operator.value;
    }
}

export class SubtractionOperator extends Operator {
    constructor(value) {
        super(value);
        this.needDigit = true;
        this.calculateWithEveryOperator = true;
    }

    calculate(operator) {
        return this.value - operator.value;
    }
}

export class MultiplyOperator extends Operator {
    constructor(value) {
        super(value);
        this.needDigit = true;
        this.calculateCurrentOperators = true;
    }

    calculate(operator) {
        return this.value * operator.value;
    }
}

export class DivisionOperator extends Operator {
    constructor(value) {
        super(value);
        this.needDigit = true;
        this.calculateCurrentOperators = true;
    }

    calculate(operator) {
        return this.changeOperatorOrder ? operator.value / this.value : this.value / operator.value;
    }
}

export class AssignmentOperator extends Operator {
    constructor(value) {
        super(value);
        this.calculateWithEveryOperator = true;
        this.updateCurrentOperator = true;
        this.needToResetAfterDigit = true;
    }
}

export class NegationOperator extends Operator {
    constructor(value) {
        super(value);
        this.onlyUpdateValue = true;
    }

    calculate() {
        return -this.value;
    }
}

export class PercentOperator extends Operator {
    constructor(value) {
        super(value);
        this.onlyUpdateValue = true;
    }

    calculate() {
        const onePercent = 0.01;
        return this.value * onePercent;
    }
}


export class ClearOperator extends Operator {
    constructor(value) {
        super(value);
        this.needToReset = true;
    }
}
