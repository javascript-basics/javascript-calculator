import {
    AssignmentOperator, ClearOperator,
    DivisionOperator,
    MultiplyOperator,
    NegationOperator,
    Operator, PercentOperator,
    SubtractionOperator,
    SumOperator
} from "./calculator_operators.js";

const DEFAULT_VALUE = 0;

const VALUE_MAX_LENGTH = 10;

const BASE_CLASS_NAME = 'Operator';

const SUM_STRING_VALUE = '+';
const SUB_STRING_VALUE = '-';
const MUL_STRING_VALUE = 'x';
const DIV_STRING_VALUE = '÷';
const ASSIGNMENT_STRING_VALUE = '=';
const NEGATION_STRING_VALUE = '±';
const PERCENT_STRING_VALUE = '%';
const CLEAR_STRING_VALUE = 'AC';
const COMMA_STRING_VALUE = '.';


export class CalculatorService {
    #activeValue = DEFAULT_VALUE;
    #cachedValue = DEFAULT_VALUE;

    #currentOperator = new Operator(DEFAULT_VALUE);
    #previousOperator = new Operator(DEFAULT_VALUE);

    #operatorMapper = new Map();

    #needResetAfterDigit = false;
    #lastButtonIsOperator = false;

    get activeValue() {
        return this.#activeValue;
    }

    get currentOperatorClassName() {
        return this.#currentOperator.constructor.name;
    }

    get previousOperatorClassName() {
        return this.#previousOperator.constructor.name;
    }

    constructor() {
        this.#initOperatorMapper();
    }

    #initOperatorMapper() {
        this.#operatorMapper.set(SUM_STRING_VALUE, (value) => new SumOperator(value));
        this.#operatorMapper.set(SUB_STRING_VALUE, (value) => new SubtractionOperator(value));
        this.#operatorMapper.set(MUL_STRING_VALUE, (value) => new MultiplyOperator(value));
        this.#operatorMapper.set(DIV_STRING_VALUE, (value) => new DivisionOperator(value));
        this.#operatorMapper.set(ASSIGNMENT_STRING_VALUE, (value) => new AssignmentOperator(value));
        this.#operatorMapper.set(NEGATION_STRING_VALUE, (value) => new NegationOperator(value));
        this.#operatorMapper.set(PERCENT_STRING_VALUE, (value) => new PercentOperator(value));
        this.#operatorMapper.set(CLEAR_STRING_VALUE, (value) => new ClearOperator(value));
    }

    handleDigit(digit) {
        const hasAlreadyComma = this.#activeValue.toString().includes(COMMA_STRING_VALUE);

        if (digit === COMMA_STRING_VALUE && hasAlreadyComma) {
            return;
        }

        const needUpdateValue = this.#currentOperator.value === this.#activeValue;
        if (needUpdateValue) {
            this.#activeValue = DEFAULT_VALUE;
        }

        if (this.#needResetAfterDigit) {
            this.#reset();
        }

        const tmpValue = this.#activeValue + digit;
        if (tmpValue.length > VALUE_MAX_LENGTH) {
            return;
        }

        this.#lastButtonIsOperator = false;

        if (digit === COMMA_STRING_VALUE) {
            this.#activeValue = tmpValue;
            return;
        }

        this.#activeValue = +tmpValue;
    }

    handleOperator(key) {
        this.#activeValue = +this.#activeValue
        const operator = this.#operatorMapper.get(key)(this.#activeValue) || new Operator(DEFAULT_VALUE);

        this.#needResetAfterDigit = operator.needToResetAfterDigit;

        if (operator.needToReset) {
            this.#reset();
            return;
        }

        if (operator.onlyUpdateValue) {
            this.#updateValue(key, operator);
            return;
        }

        if (this.#lastButtonIsOperator && operator.needDigit) {
            this.#currentOperator = operator;
            return;
        }

        if (operator.calculateWithEveryOperator) {
            this.#calculateWithEveryOperator(operator);
        }

        const needToCalculateCurrentOperators = operator.calculateCurrentOperators &&
            this.#currentOperator.calculateCurrentOperators;

        if (needToCalculateCurrentOperators) {
            this.#calculateCurrOperators(operator);
        }

        this.#updateOperators(key, operator);
        this.#roundValue();
        this.#lastButtonIsOperator = true;
    }

    #reset() {
        this.#activeValue = DEFAULT_VALUE;
        this.#currentOperator = new Operator(DEFAULT_VALUE);
        this.#previousOperator = new Operator(DEFAULT_VALUE);
    }

    #updateValue(key, operator) {
        if (this.#lastButtonIsOperator) {
            this.#activeValue = (this.#operatorMapper.get(key)(DEFAULT_VALUE) || new Operator(DEFAULT_VALUE)).calculate();
            return;
        }

        this.#activeValue = operator.calculate();
        this.#roundValue();
    }

    #calculateCurrOperators(operator) {
        this.#activeValue = this.#currentOperator.calculate(operator);
        this.#currentOperator = new Operator(DEFAULT_VALUE);
    }

    #calculateWithEveryOperator(operator) {
        const calculateAllOperators = this.previousOperatorClassName !== BASE_CLASS_NAME &&
            this.currentOperatorClassName !== BASE_CLASS_NAME;
        const calculateCurrentOperators = this.currentOperatorClassName !== BASE_CLASS_NAME;

        if (calculateAllOperators) {
            this.#activeValue = this.#previousOperator.calculate(new Operator(this.#currentOperator.calculate(operator)));
        } else if (calculateCurrentOperators) {
            this.#activeValue = this.#currentOperator.calculate(operator);
        }

        this.#previousOperator = new Operator(DEFAULT_VALUE);
    }

    #updateOperators(key, operator) {
        const needToStoreOperator = this.currentOperatorClassName !== BASE_CLASS_NAME &&
            !this.#currentOperator.calculateCurrentOperators && !operator.calculateWithEveryOperator;

        if (needToStoreOperator) {
            this.#previousOperator = this.#currentOperator;
        }

        if (!this.#cachedValue) {
            this.#cachedValue = operator.value;
        }

        if (operator.updateCurrentOperator) {
            this.#currentOperator.value = this.#cachedValue;
            this.#currentOperator.changeOperatorOrder = true;
            this.#previousOperator = new Operator(DEFAULT_VALUE);
            return;
        }

        this.#currentOperator = this.#operatorMapper.get(key)(this.#activeValue);
        this.#cachedValue = null;
    }

    #roundValue() {
        this.#activeValue = +this.#activeValue.toFixed(VALUE_MAX_LENGTH);
    }
}
